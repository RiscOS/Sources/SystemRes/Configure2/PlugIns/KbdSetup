# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for KbdSetup
#

COMPONENT  = KbdSetup
override TARGET = !RunImage
INSTTYPE   = app
OBJS       = Keyboards Main Settings ToolboxE WimpE WimpM
LIBS       = ${CONLIB} ${EVENTLIB} ${TBOXLIB} ${WIMPLIB}
CINCLUDES  = ${CONINC} ${TBOXINC}
INSTAPP_FILES = !Boot !Run !RunImage Messages Res \
                !Sprites !Sprites11 !Sprites22 CoSprite CoSprite11 CoSprite22 \
                Ursula.!Sprites:Ursula Ursula.CoSprite:Ursula
INSTAPP_VERSION = Messages

include CApp

C_WARNINGS = -fa

# Dynamic dependencies:
